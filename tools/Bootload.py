import argparse
import can
import struct
import time
from enum import Enum, IntEnum, auto
from can.message import Message
import can.interfaces.canalystii as canalystii
import progressbar
import datetime

class TargetDevice(Enum):
    SPEAKER = "SPEAKER"
    STAND   = "STAND"

class CanMessages(IntEnum):
  RESTART_SPEAKER_CONTROL_BOOTLOAD = 0
  RESTART_STAND_CONTROL_BOOTLOAD   = 1
  PROGRAM_SPEAKER_CONTROL          = 2
  PROGRAM_STAND_CONTROL            = 3
  SPEAKER_CONTROL_PROGRAM_STATUS   = 4
  STAND_CONTROL_PROGRAM_STATUS     = 5
  SPEAKER_CONTROL_REBOOT           = 6
  STAND_CONTROL_REBOOT             = 7
  SPEAKER_CONTROL_STATUS           = 100
  SPEAKER_CONTROL_COMMAND          = 101
  STAND_CONTROL_STATUS_ENCODERS    = 102
  STAND_CONTROL_STATUS_MOTORS      = 103
  STAND_CONTROL_COMMAND            = 104



class Bootloader(can.Listener):
    class Status(Enum):
        WAITING_FOR_COMMUNICATION = "WAITING FOR COMMUNICATION"
        REBOOTING                 = "REBOOTING"
        BOOTLOADING               = "BOOTLOADING"

    MESSAGES_TYPES = {
        TargetDevice.SPEAKER : {
            "MAINAPP_STATUS"    : CanMessages.SPEAKER_CONTROL_STATUS,
            "BOOTLOADER_STATUS" : CanMessages.STAND_CONTROL_PROGRAM_STATUS,
            "RESTART_BOOTLOAD"  : CanMessages.RESTART_SPEAKER_CONTROL_BOOTLOAD,
            "REBOOT"            : CanMessages.SPEAKER_CONTROL_REBOOT,
            "PROGRAM_DATA"      : CanMessages.PROGRAM_SPEAKER_CONTROL,
        },
        TargetDevice.STAND : {
            "MAINAPP_STATUS"    : CanMessages.STAND_CONTROL_STATUS_ENCODERS,
            "BOOTLOADER_STATUS" : CanMessages.STAND_CONTROL_PROGRAM_STATUS,
            "RESTART_BOOTLOAD"  : CanMessages.RESTART_STAND_CONTROL_BOOTLOAD,
            "REBOOT"            : CanMessages.STAND_CONTROL_REBOOT,
            "PROGRAM_DATA"      : CanMessages.PROGRAM_STAND_CONTROL,

        }
    }
    START_ADDRESS = 0xA000

    def __init__(self, target : TargetDevice, firmware_path, candevice : can.interface.Bus):
        super().__init__()

        self.target = target
        self.firmware = open(firmware_path, "rb").read()
        self.firmware_path = firmware_path
        self.bus = candevice
        self.status = self.Status.WAITING_FOR_COMMUNICATION
        self.progress = self.START_ADDRESS
        self.recievedProgress = self.START_ADDRESS
        self.estimatedRequiredMessages = self.computeMessageCount(firmware_path)
        self.bar = progressbar.ProgressBar(min_value = 0, max_value = self.estimatedRequiredMessages)

    def sendReboot(self) -> None:
        if self.status in (self.Status.WAITING_FOR_COMMUNICATION, self.Status.REBOOTING):
            self.bus.send(can.Message(arbitration_id=self.MESSAGES_TYPES[self.target]["REBOOT"], is_extended_id=False))

    @classmethod
    def getBulkCount(cls, firmware : bytes, index : int, size : int):
        count = 1
        data = firmware[index:index + size]

        for i in range(index + size, len(firmware), size):
            cmpData = firmware[i:i + size]
            if cmpData == data:
                count+=1
                if count >= 255:
                    return count
            else:
                return count
        return count

    @classmethod
    def computeMessageCount(cls, firmware_path : str, startIndex = 0) -> int:
        msgCount = 0
        progress = startIndex
        firmware = open(firmware_path, "rb").read()

        while progress < len(firmware):
            for i in range(1, 3):
                bulkcount, repeat = cls.getBulkChunk(firmware, progress)
                if bulkcount:
                    progress += bulkcount * repeat
                    msgCount += 1
                    continue

            progress += 5
            msgCount += 1
        return msgCount
    
    @classmethod
    def getBulkChunk(cls, firmware, index):
        for i in range(1, 4)[::-1]:
            foundBulk = False
            bulkCount = cls.getBulkCount(firmware, index, i)
            if bulkCount > 5 / i:
                foundBulk = True
                break
        if foundBulk:
            return i, bulkCount
        return 0, 0

    def handleStatus(self, msg : Message) -> None:
        self.recievedProgress = struct.unpack(">I", msg.data[:4])[0]
        if(self.recievedProgress >= self.START_ADDRESS + len(self.firmware) and self.status != self.Status.BOOTLOADING):
            self.sendReboot()
        else:
            self.progress = self.recievedProgress
            self.status = self.Status.BOOTLOADING
        
    def on_message_received(self, msg: Message) -> None:
        if msg.arbitration_id == self.MESSAGES_TYPES[self.target]["MAINAPP_STATUS"]:
            self.sendReboot()
        if msg.arbitration_id == self.MESSAGES_TYPES[self.target]["BOOTLOADER_STATUS"]:
            self.handleStatus(msg)

    def generateBulkData(self, bytecount, repeat):
        index = self.progress - self.START_ADDRESS
        data = bytes([repeat])
        data += self.firmware[index:index + bytecount]
        return data

    def generateStandardData(self):
        index = self.progress - self.START_ADDRESS
        data = self.firmware[index:index + 5]
        while len(data) < 5:
            data += bytes([0])
        return data

    def sendFirmware(self):
        index = self.progress - self.START_ADDRESS
        data = struct.pack(">I", self.progress)[1:]

        index = self.progress - self.START_ADDRESS
        bulkcount, repeat = self.getBulkChunk(self.firmware, index)
        if bulkcount:
            data += self.generateBulkData(bulkcount, repeat)
            self.progress += bulkcount * repeat
        else:
            data += self.generateStandardData()
            self.progress += 5

        msg = can.Message(arbitration_id=self.MESSAGES_TYPES[self.target]["PROGRAM_DATA"], data = data, is_extended_id=False)
        self.bus.send(msg)

    def update(self) -> None:
        lastProgressUpdate = datetime.datetime.now()
        print(hex(self.START_ADDRESS + len(self.firmware)))
        while self.recievedProgress < self.START_ADDRESS + len(self.firmware):
            if self.status in [self.Status.BOOTLOADING] and self.progress < self.START_ADDRESS + len(self.firmware):
                self.sendFirmware()
                time.sleep(0.001)
            if datetime.datetime.now() > lastProgressUpdate + datetime.timedelta(milliseconds=200):
                lastProgressUpdate = datetime.datetime.now()
                self.bar.update(self.estimatedRequiredMessages - self.computeMessageCount(self.firmware_path, self.recievedProgress - self.START_ADDRESS))



        

def __main__():
    parser = argparse.ArgumentParser()
    parser.add_argument("--target", help="target device to bootload", choices = ["STAND", "SPEAKER"],
                        type=str, required=True)
    parser.add_argument("--firmware", help="Firmware file to bootload", type=str, required=True)
    parser.add_argument("--msg_count", help="Just count the messages needed to bootload", action='store_true')

    parser.add_argument("--can", help="CAN device to use", type=str, default="/dev/ttyACM0")
    parser.add_argument("--bitrate", help="CAN bitrate", type=int, default=50000)

    args = parser.parse_args()

    if(args.msg_count):
        print("Messages needed:", Bootloader.computeMessageCount(args.firmware))
        return
    
    bus = canalystii.CANalystIIBus(channel=1, bitrate=125000)
    bootloader = Bootloader(TargetDevice(args.target), args.firmware, bus)
    can.Notifier(bus, [bootloader])
    bootloader.update()
    bootloader.stop()
    bus.stop_all_periodic_tasks()
    bus.shutdown()

if __name__ == "__main__":
    __main__()