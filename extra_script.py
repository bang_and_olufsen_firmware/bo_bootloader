from os import makedirs
from os.path import isdir, join, dirname, basename
import zlib
import struct
import subprocess
Import('env')


def create_output(target, source, env):
    subprocess.run([env["OBJCOPY"], "-O", "binary", "-R", ".eeprom", env.subst("$BUILD_DIR/${PROGNAME}.elf"), env.subst("$BUILD_DIR/${PROGNAME}_crc.bin")])

    lengths = {
        "stand" : 0x400 * 40 - 4,
        "speaker" : 0x400 * 40 - 4,
        "stand_bootupdater" : 0x400 * 88 - 4,
        "speaker_bootupdater" : 0x400 * 88 - 4,
    }
    build_type = basename(dirname(env.subst("$BUILD_DIR/${PROGNAME}_crc.bin")))
    data = open(env.subst("$BUILD_DIR/${PROGNAME}_crc.bin"), "rb").read()[:lengths[build_type]]
    crc = zlib.crc32(data)
    with open(env.subst("$BUILD_DIR/${PROGNAME}_crc.bin"), "wb") as f:
        f.write(data)
        print("Appending CRC: ", env.subst("$BUILD_DIR/${PROGNAME}_crc.bin"), hex(crc))
        f.write(struct.pack("<I", crc))
        f.flush()


env.AddPostAction("$BUILD_DIR/${PROGNAME}.elf", create_output)

env.AddPostAction(
    "$BUILD_DIR/${PROGNAME}.elf",
    env.VerboseAction(" ".join([
        "$OBJCOPY", "-O", "ihex", "-R", ".eeprom",
        "$BUILD_DIR/${PROGNAME}.elf", "$BUILD_DIR/${PROGNAME}.hex"
    ]), "Building $BUILD_DIR/${PROGNAME}.hex")
)


    