#include <inttypes.h>

#include "common/AppTypes.h"
#include "common/Bootloader/Bootloader.hpp"
#include "common/CANMessages.hpp"
#include "common/EventTimer/EventTimer.hpp"
#include "common/PCBType.hpp"
#include "common/TJA1055/TJA1055.hpp"
#include "common/bo_pins.h"
#include "common/setFrequency/setFrequency.hpp"
#include "mbed.h"

static CanMessageType getBootloaderStatusType();
#if !defined(BOOTUPDATER_STAND) && !defined(BOOTUPDATER_SPEAKER)
static void subscribe();
#endif
static void sendBootloaderStatus();
static void handleProgram(CANMessage& msg);
static void canTask();
static void handleRestart(CANMessage& msg);
static void resetCallback(CANMessage& msg);

__attribute__((section("AHBSRAM0"))) static uint8_t CAN_TASK_STACK[2048];

static constexpr uint32_t JUMP_WAITTIME_MS = 1000;

static TJA1055     can(CAN_RX, CAN_TX, CAN_ERR_N, CAN_STB, CAN_EN);
static Bootloader* bootloader;
static uint64_t    jumpTime = 0;
static EventTimer  event_timer;

#ifdef DEBUG_PRINT
namespace mbed
{
static BufferedSerial console(P2_0, NC, 115200);
FileHandle*           mbed_override_console(int fd)
{
  return &console;
}
}  // namespace mbed
#endif

CanMessageType getBootloaderStatusType()
{
#if defined(SPEAKER)
  return CanMessageType::SPEAKER_CONTROL_PROGRAM_STATUS;
#elif defined(STAND)
  return CanMessageType::STAND_CONTROL_PROGRAM_STATUS;
#else
  #error "NO BUILDTYPE DEFINED"
#endif
}

#if !defined(BOOTUPDATER_STAND) && !defined(BOOTUPDATER_SPEAKER)
void subscribe(void)
{
  #if defined(SPEAKER)
  can.registerCallback(CanMessageType::RESTART_SPEAKER_CONTROL_BOOTLOAD, handleRestart);
  can.registerCallback(CanMessageType::PROGRAM_SPEAKER_CONTROL, handleProgram);
  can.registerCallback(CanMessageType::SPEAKER_CONTROL_REBOOT, resetCallback);
  can.registerCallback(CanMessageType::SPEAKER_JUMP_TO_MAINAPP, []([[maybe_unused]] CANMessage&) {
    jumpTime = get_ms_count();
  });

  #elif defined(STAND)
  can.registerCallback(CanMessageType::RESTART_STAND_CONTROL_BOOTLOAD, handleRestart);
  can.registerCallback(CanMessageType::PROGRAM_STAND_CONTROL, handleProgram);
  can.registerCallback(CanMessageType::STAND_CONTROL_REBOOT, resetCallback);
  can.registerCallback(CanMessageType::STAND_JUMP_TO_MAINAPP, []([[maybe_unused]] CANMessage&) {
    jumpTime = get_ms_count();
  });

  #else
    #error "NO BUILDTYPE DEFINED"
  #endif
}
#endif

void sendBootloaderStatus()
{
  uint8_t data[8];
  *(reinterpret_cast<uint32_t*>(data))     = __builtin_bswap32(bootloader->getProgress());
  *(reinterpret_cast<uint32_t*>(data + 4)) = __builtin_bswap32(bootloader->get_application_crc());

  can.write(mbed::CANMessage(
    static_cast<unsigned int>(getBootloaderStatusType()),
    data,
    sizeof(data),
    CANType::CANData,
    CANFormat::CANStandard));
}

void handleProgram(CANMessage& msg)
{
  uint32_t address = msg.data[0];
  address <<= 8;
  address += msg.data[1];
  address <<= 8;
  address += msg.data[2];

  const uint8_t* data       = reinterpret_cast<const uint8_t*>(&msg.data[3]);
  uint8_t        dataLength = 5;
  uint8_t        repeat     = 1;

  if(msg.len != 8)
  {
    repeat     = msg.data[3];
    dataLength = msg.len - 4;
    data       = reinterpret_cast<const uint8_t*>(&msg.data[4]);
  }

  if(!bootloader->addData(address, data, dataLength, repeat))
  {
    sendBootloaderStatus();
  }
}

void checkShouldJump()
{
  if(bootloader->isValid())
  {
    if(!(bootloader->getApptype() == AppType::BOOTLOADER_UPDATER
         && bootloader->getBootloaderCRC() == bootloader->getBootloaderUpdaterCRC()))
    {
#ifdef DEBUG_PRINT
      printf("JUMPING TO APPLICATION!\n");
      fflush(stdout);
      thread_sleep_for(50);
#endif
      bootloader->jump();
    }
  }
}

void canTask()
{
  jumpTime = 0xFFFFFFFFFFFFFFFFU;

  while(1)
  {
    Watchdog::get_instance().kick();
    can.recieveMessages();
    event_timer.step();

    if(jumpTime <= get_ms_count())
    {
      checkShouldJump();
    }
  }
}

void handleRestart(CANMessage& msg)
{
  bootloader->restart();
}

void resetCallback([[maybe_unused]] CANMessage& msg)
{
  NVIC_SystemReset();
}

__attribute__((used)) int main()
{
  setFrequency();
  bootloader = new Bootloader([]() { jumpTime = get_ms_count(); });

  const uint32_t WDOG_TIMEOUT_MS = 2500;
  Watchdog&      watchdog        = Watchdog::get_instance();
  watchdog.start(WDOG_TIMEOUT_MS);

#if !defined(BOOTUPDATER_STAND) && !defined(BOOTUPDATER_SPEAKER)
  subscribe();
#else
  extern char __start_bootloader_binary__;
  extern char __end_bootloader_binary__;
  uint32_t    bootloader_binary_size = (uint32_t)(&__end_bootloader_binary__ - &__start_bootloader_binary__);

  for(uint32_t addr = 0x400; addr < bootloader_binary_size; addr += 128)
  {
  #ifdef DEBUG_PRINT
    printf("BOOTUPDATER START %p 0x%08lX\n", ((uint8_t*)&__start_bootloader_binary__) + addr, bootloader_binary_size);
  #endif
    bootloader->addData(addr, ((uint8_t*)&__start_bootloader_binary__) + addr, 128, 1U);
  #ifdef DEBUG_PRINT
    printf("BOOTUPDATER WRITTEN!\n");
  #endif
    thread_sleep_for(40);
    Watchdog::get_instance().kick();
  }
  bootloader->jump();
#endif

  event_timer.addEvent(sendBootloaderStatus, get_ms_count() + 0, 100);
#ifdef DEBUG_PRINT
  event_timer.addEvent([]() { printf("ALIVE BOOTLOADER! VALID: %d\n", bootloader->isValid()); }, get_ms_count() + 5, 200);
#endif

  Thread* can_thread = new Thread(osPriorityNormal, sizeof(CAN_TASK_STACK), CAN_TASK_STACK, "can_thread");
  can_thread->start(canTask);

  while(1)
  {
    thread_sleep_for(1000000);
  }

  return 0;
}
