#pragma once
#include "mbed.h"

// CAN Transciever
#define IC1_PIN6  P2_8
#define IC1_PIN5  P2_9
#define IC1_PIN4  P1_9
#define IC1_PIN3  P0_0
#define IC1_PIN2  P0_1
#define CAN_TX    IC1_PIN2
#define CAN_RX    IC1_PIN3
#define CAN_ERR_N IC1_PIN4
#define CAN_STB   IC1_PIN5
#define CAN_EN    IC1_PIN6